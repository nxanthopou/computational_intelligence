#Xanthopoulos Nikitas
#28/08/2016
#Ergasia gia to ma8ima Ypologistiki 
#Noimosyni
#euresi monopatiou me xrisi
#grammatikis ekseliksis 



push!(LOAD_PATH, "")
using GrammaticalEvolution
import GrammaticalEvolution.evaluate!
import GrammaticalEvolution.isless
import Base.copy
import Base.+
import Base.-

include("tmp_map.jl")
include("lib_map.jl")
include("lib_crawler.jl")
include("lib_crawler_individual.jl")
include("lib_crawler_population.jl")

const new_cell = tmp_map.pcv
const wall = tmp_map.wcv
const target_cell = tmp_map.tcv
const discovered = tmp_map.dcv
const n_population = 50

function read_map()
    map = tmp_map.minimap
    start_pos = tmp_map.start_position
    end_pos = tmp_map.end_position

    return (map, start_pos, end_pos)
end

copy(map::Map) = Map(copy(map.value), map.start_pos, map.end_pos)

function evaluate!(grammar::Grammar, ind::CrawlerIndividual, map::Map)
  map = copy(map)

  crawler = Crawler(map.start_pos[1], map.start_pos[2])
  crawler.map = map.value
  try
    ind.code = transform(grammar, ind)
     @eval fn(map::Map, crawler::Crawler) = $(ind.code)
  catch e
   if typeof(e) !== MaxWrapException
        println(e)
   end
   ind.fitness = -Inf
   return
  end

  fn(map, crawler)
  ind.invalid_moves = crawler.invalid_moves
  ind.map = crawler.map
  ind.x = crawler.x
  ind.y = crawler.y
#   ind.fitness = convert(Float64, crawler.eaten) - convert(Float64, crawler.moves)*10 - convert(Float64, crawler.checks)*50
  ind.fitness = convert(Float64, crawler.discovered) - convert(Float64, crawler.moves)*12 - convert(Float64, crawler.invalid_moves)*15
end

isless(ind1::CrawlerIndividual, ind2::CrawlerIndividual) = ind1.fitness > ind2.fitness

make_block(lst::Array) = Expr(:block, lst...)


macro make_call(fn, args...)
  Expr(:call, fn, args...)
end


@grammar crawler_grammar begin
  start = block
  
  moves = move_forward | move_backward | move_left | move_right
  
  move_left = Expr(:call, :move_left, :crawler)
  move_right = Expr(:call, :move_right, :crawler)
  move_forward = Expr(:call, :move_forward, :crawler)
  move_backward = Expr(:call, :move_backward, :crawler)
  
  block[make_block] = moves^(1:35)
end


function main()
  (map, start_pos, end_pos) = read_map()
  
  map = Map(map, start_pos, end_pos)

  pop = CrawlerPopulation(n_population, size(tmp_map.minimap, 1)*size(tmp_map.minimap, 2)*2)

  fitness = 0
  generation = 1

  evaluate!(crawler_grammar, pop, map)
  while generation < 1000
    pop = generate(crawler_grammar, pop, 0.2, 0.5, map)
#     map.value = pop[1].map
    max_fitness = pop[1].fitness
    min_fitness = pop[length(pop)].fitness
    println("generation: $generation, $(length(pop)), max fitness=$max_fitness, min fitness=$min_fitness")
    
    println(pop[1].code)
    println(pop[1].map)
    println(pop[1].invalid_moves)
    if [ pop[1].x ; pop[1].y ] == tmp_map.end_position && (pop[1].invalid_moves == 0) 
      break
    end
    generation += 1
  end
end

 function move_forward(crawler)
    try
       crawler.y +=1
       crawler.discovered += crawler.map[crawler.x, crawler.y]
       if crawler.map[crawler.x , crawler.y] == tmp_map.wcv
        crawler.invalid_moves += 1
       end
       crawler.map[crawler.x, crawler.y] = tmp_map.dcv
       if tmp_map.end_position == [crawler.x ; crawler.y]
#          crawler.discovered += target_cell
         crawler.target_cell = true
       end    
    catch LoadError
#        crawler.discovered += wall
    end
    crawler.moves +=1
  end
  
  function move_backward(crawler)
    try

	      crawler.y -=1
	      crawler.discovered += crawler.map[crawler.x, crawler.y]
          if crawler.map[crawler.x , crawler.y] == tmp_map.wcv
            crawler.invalid_moves += 1
          end
	      crawler.map[crawler.x, crawler.y] = tmp_map.dcv
          if tmp_map.end_position == [crawler.x ; crawler.y]
#             crawler.discovered += target_cell
            crawler.target_cell = true
          end
    catch LoadError
#       crawler.discovered += wall
    end
    crawler.moves +=1
  end
  
  function move_left(crawler)
    try
            crawler.x -=1
            crawler.discovered += crawler.map[crawler.x, crawler.y]
            if crawler.map[crawler.x , crawler.y] == tmp_map.wcv
                crawler.invalid_moves += 1
            end
            crawler.map[crawler.x, crawler.y] = tmp_map.dcv

            if tmp_map.end_position == [crawler.x ; crawler.y]
#               crawler.discovered += target_cell
              crawler.target_cell = true
            end
    catch LoadError
#       crawler.discovered += wall
    end
    crawler.moves +=1
  end
  
  function move_right(crawler)
    try
            crawler.x +=1
            crawler.discovered += crawler.map[crawler.x, crawler.y]
            if crawler.map[crawler.x , crawler.y] == tmp_map.wcv
                crawler.invalid_moves += 1
            end
            crawler.map[crawler.x, crawler.y] = tmp_map.dcv
            if tmp_map.end_position == [crawler.x ; crawler.y]
#               crawler.discovered += target_cell
              crawler.target_cell = true
            end
    catch LoadError
#       crawler.discovered += wall
    end
    crawler.moves +=1
  end


main()
