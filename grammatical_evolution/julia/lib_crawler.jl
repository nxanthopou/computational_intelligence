type Crawler
  x::Int64
  y::Int64
  eaten::Int64
   
  cells_explored::Int64
  moves::Int64
  invalid_moves::Int64
  next_move::Int64
  valid_move::Bool
  map::Array{Int64, 2}
  next_cell::Array{Int64, 1}
  target_cell_found::Bool
  checks::Int64
  discovered::Int64
  
  function Crawler(x::Int64, y::Int64)
    return new(x, y, 0, 0, 0, 0, 0, false, zeros(5,7), [0,0], false, 0,0)
  end
end
