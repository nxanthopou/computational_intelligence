#Xanthopoulos Nikitas
#28/08/2016
#Ergasia gia to ma8ima Ypologistiki 
#Noimosyni
#euresi monopatiou me xrisi
#genetikou programmatismou

module gpp
  include("crawler.jl")

  const max_pop = 150
  const new_cell = ci_map.pcv
  const wall = ci_map.wcv
  const target_cell = ci_map.tcv
  const discovered = ci_map.dcv
  const bad_move_bonus = -1
  population_n = 1
  
  function create_entity()
	Crawler(rand(1:4, (size(ci_map.minimap, 1)*size(ci_map.minimap, 2)))) 
  end

  function fitness(ent)
    ent.code = Expr(:block, :( crawler = $ent))
    push!(ent.cell_history, [ent.x ; ent.y])
    for i=1:size(ent.chromosome, 1)
        if ent.chromosome[i] == 1
          push!(ent.code.args, :(move_forward(crawler)))
#           move_forward(ent)
        elseif ent.chromosome[i] == 2
          push!(ent.code.args, :(move_backward(crawler)))
#           move_backward(ent)
        elseif ent.chromosome[i] == 3
          push!(ent.code.args, :(move_left(crawler)))
#           move_left(ent)
        elseif ent.chromosome[i] == 4
          push!(ent.code.args, :(move_right(crawler)))
#           move_right(ent)
        end
 
    end
    ent.code.args = ent.code.args[1:ent.target_hit]
    eval(ent.code)
    end_index = size(ent.cell_history, 1)

#     ent.code.args = ent.code.args[1:(end_index+1)]
    return ent.discovered - ent.moves
  end
  
  function group_entities(pop)
    global population_n
#     println(pop[1].map)
#     println(pop[1].target_cell)
#     println(pop[1].fitness)
#     println(population_n)
    println(population_n, pop[1].fitness, pop[1].target_cell, size(pop[1].code.args,1))
    if population_n >= max_pop 
      cells_to_moves(pop[1])
        for i=1:size(pop[1].map, 1)*size(pop[1].map, 2)
          if pop[1].map[i] == discovered
            pop[1].map[i] = 0
          else
            pop[1].map[i] = 1
          end
        end
     
       println("Discovered map: \n", pop[1].map)
#       println(unique(pop[1].cell_history))
       println(pop[1].cell_history)
      return 
    end
	for i in 1:length(pop)
	  produce([1, i])
	end
    population_n += 1
  end


  function crossover(group)
	child = Crawler()
	num_parents = length(group)
	for i in 1:length(group[1].chromosome)
	  parent = (rand(UInt) % num_parents) + 1
	  child.chromosome[i] = group[parent].chromosome[i]
	  child.map[i] = group[parent].map[i]
	end

	child
  end

  function mutate(ent)
	rand(Float64) < 0.98 && return

	some_elements = Int(floor(size(ci_map.minimap, 1)*size(ci_map.minimap, 2)/2))
	for i = 1:some_elements
	  rand_element = rand(1:(size(ci_map.minimap, 1)*size(ci_map.minimap, 2)))
	  ent.chromosome[rand_element ] = rand(1:4)
	end
  end

  function cells_to_moves(ent)
    c_map = unique(ent.cell_history)
    end_index = 0

	push!(ent.genetic_moves, "begin")
	for i=1:(size(c_map, 1) - 1)
	  if c_map[i+1][1] == c_map[i][1] + 1 && c_map[i+1][2] == c_map[i][2]
        push!(ent.genetic_moves, " move_right(ent)")
	  elseif c_map[i+1][1] == c_map[i][1] - 1 && c_map[i+1][2] == c_map[i][2]
        push!(ent.genetic_moves, " move_left(ent)")
	  elseif c_map[i+1][1] == c_map[i][1] && c_map[i][2] + 1 == c_map[i+1][2]
        push!(ent.genetic_moves, " move_forward(ent)")
	  elseif c_map[i+1][1] == c_map[i][1] && c_map[i][2] - 1 == c_map[i+1][2]
        push!(ent.genetic_moves, " move_backward(ent)")
	  end
	end
	push!(ent.genetic_moves, "end")
	println("*********Start of programm*********")
#     for i=1:size(ent.genetic_moves,1)
#       println(ent.genetic_moves[i])
#     end
#     println(ent.code)
	println("*********End of programm*********")
	return ent.genetic_moves
   end

   
   function move_forward(crawler)
    if crawler.y + 1 <= size(ci_map.minimap, 2) && crawler.map[ crawler.x, crawler.y + 1] >= crawler.map[ crawler.x, crawler.y]
            crawler.y +=1
            crawler.discovered += crawler.map[crawler.x, crawler.y]
            crawler.map[crawler.x, crawler.y] = ci_map.dcv
            push!(crawler.cell_history, [crawler.x, crawler.y])
            crawler.moves +=1
    else 
       crawler.discovered += bad_move_bonus
    end
    
    if [crawler.x ; crawler.y] == ci_map.end_position && crawler.target_cell == false
      crawler.target_cell = true
      crawler.target_hit = size(ci_map.minimap, 1)*size(ci_map.minimap, 2) - countnz(crawler.map)
      crawler.discovered += target_cell
    end
  end
  
  function move_backward(crawler)
    if crawler.y - 1 >= 1  && crawler.map[ crawler.x, crawler.y - 1] >= crawler.map[ crawler.x, crawler.y]
            crawler.y -=1
            crawler.discovered += crawler.map[crawler.x, crawler.y]
            crawler.map[crawler.x, crawler.y] = ci_map.dcv
            push!(crawler.cell_history, [crawler.x, crawler.y])
            crawler.moves +=1
    else 
       crawler.discovered += bad_move_bonus
    end
    
    if [crawler.x ; crawler.y] == ci_map.end_position && crawler.target_cell == false
      crawler.target_cell = true
      crawler.target_hit = size(ci_map.minimap, 1)*size(ci_map.minimap, 2) - countnz(crawler.map)
      crawler.discovered += target_cell
    end
  end
  
  function move_left(crawler)
    if crawler.x - 1 >= 1 && crawler.map[ crawler.x - 1, crawler.y] >= crawler.map[ crawler.x, crawler.y]
            crawler.x -=1
            crawler.discovered += crawler.map[crawler.x, crawler.y]
            crawler.map[crawler.x, crawler.y] = ci_map.dcv
            push!(crawler.cell_history, [crawler.x, crawler.y])
            crawler.moves +=1
    else 
       crawler.discovered += bad_move_bonus
    end
    
    if [crawler.x ; crawler.y] == ci_map.end_position && crawler.target_cell == false
      crawler.target_cell = true
      crawler.target_hit = size(ci_map.minimap, 1)*size(ci_map.minimap, 2) - countnz(crawler.map)
      crawler.discovered += target_cell
    end
  end
  
  function move_right(crawler)
    if crawler.x + 1 <= size(ci_map.minimap, 1) && crawler.map[ crawler.x + 1, crawler.y] >= crawler.map[ crawler.x, crawler.y]

            crawler.x +=1
            crawler.discovered += crawler.map[crawler.x, crawler.y]
            crawler.map[crawler.x, crawler.y] = ci_map.dcv
            push!(crawler.cell_history, [crawler.x, crawler.y])
            crawler.moves +=1
    else 
       crawler.discovered += bad_move_bonus
    end
    
    if [crawler.x ; crawler.y] == ci_map.end_position && crawler.target_cell == false
      crawler.target_cell = true
      crawler.target_hit = size(ci_map.minimap, 1)*size(ci_map.minimap, 2) - countnz(crawler.map)
      crawler.discovered += target_cell
    end
  end

end 
