#Xanthopoulos Nikitas
#02/05/2016
#Ergasia gia to ma8ima Ypologistiki 
#Noimosyni
#xartis kai metadata tou

module ci_map
  start_position = [2; 1]
  end_position = [3; 7]
  wcv = -5 #wall cell value
  pcv = 2 #path cell value
  tcv = 5 #target cell value
  dcv = 0 #discovered cell value
  minimap = [ wcv wcv wcv wcv wcv wcv wcv;
              dcv pcv wcv pcv pcv pcv wcv;
			  wcv pcv wcv pcv wcv pcv pcv;
			  wcv pcv pcv pcv pcv pcv wcv;
			  wcv wcv wcv wcv wcv wcv wcv;
		]
end
