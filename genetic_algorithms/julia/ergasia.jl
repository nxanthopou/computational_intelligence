#Xanthopoulos Nikitas
#02/05/2016
#Ergasia gia to ma8ima Ypologistiki 
#Noimosyni
#Kentriko module gia tin ektelesi
#tou algori8mou

module ergasia
  include("crawler.jl")

  function create_entity()
	Crawler(rand(1:4, 1,(size(ci_map.minimap, 1)*size(ci_map.minimap, 2)))) #array 1x35 me tyxaious ari8mous apo 1 ews 4 
  end

  function fitness(ent)
	current_cell = [ ci_map.start_position[1], ci_map.start_position[2]]
	score = 0

	for i = 1:(size(ent.chromosome, 1)) 
	  next_move = ent.chromosome[i]
	  if next_move == 1 && current_cell[1] + 1 <= size(ci_map.minimap, 1) && ci_map.minimap[current_cell[1] + 1, current_cell[2]] == 0 #
		current_cell[1] = current_cell[1] + 1
		push!(ent.chromosome_map, (current_cell[1], current_cell[2]))
		ent.steps = ent.steps + 1
	  elseif next_move == 2 && current_cell[1] - 1 > 0 && ci_map.minimap[current_cell[1] - 1, current_cell[2]] == 0
		current_cell[1] = current_cell[1] - 1
		push!(ent.chromosome_map, (current_cell[1], current_cell[2]))
		ent.steps = ent.steps + 1
	  elseif next_move == 3 && current_cell[2] + 1 <= size(ci_map.minimap, 2) && ci_map.minimap[current_cell[1], current_cell[2] + 1] == 0
		current_cell[2] = current_cell[2] + 1
		push!(ent.chromosome_map, (current_cell[1], current_cell[2]))
		ent.steps = ent.steps + 1
	  elseif next_move == 4 && current_cell[2] - 1 > 0 && ci_map.minimap[current_cell[1], current_cell[2] - 1] == 0
		current_cell[2] = current_cell[2] - 1
		push!(ent.chromosome_map, (current_cell[1], current_cell[2]))
		ent.steps = ent.steps + 1
	  end
	end
	ent.chromosome_map = unique(ent.chromosome_map)

	get_this_bonus = 0
	if (ent.chromosome_map[1][1] == ci_map.start_position[1] && ent.chromosome_map[1][2] == ci_map.start_position[2]) && ent.steps >= 1
	  get_this_bonus += 0.4
	  if size(ent.chromosome_map, 1) > 1 && ( ( ent.chromosome_map[1][1] == ent.chromosome_map[2][1] + 1  && 
		  ent.chromosome_map[1][2] == ent.chromosome_map[2][2]) ||
		  (ent.chromosome_map[1][1] == ent.chromosome_map[2][1] - 1 && 
		  ent.chromosome_map[1][2] == ent.chromosome_map[2][2]) ||
		  (ent.chromosome_map[1][2] == ent.chromosome_map[2][2] + 1 && 
		  ent.chromosome_map[1][1] == ent.chromosome_map[2][1]) ||
		  (ent.chromosome_map[1][2] == ent.chromosome_map[2][2] - 1 && 
		  ent.chromosome_map[1][1] == ent.chromosome_map[2][1]) )

		get_this_bonus += 0.4
	  end
	end

	if size(ent.chromosome_map , 1 ) > 2 && ( ( ent.chromosome_map[end][1] == ent.chromosome_map[end-1][1] + 1  && 
		ent.chromosome_map[end][2] == ent.chromosome_map[end-1][2]) ||
		(ent.chromosome_map[end][1] == ent.chromosome_map[end-1][1] - 1 && 
		ent.chromosome_map[end][2] == ent.chromosome_map[end-1][2]) ||
		(ent.chromosome_map[end][2] == ent.chromosome_map[end-1][2] + 1 && 
		ent.chromosome_map[end][1] == ent.chromosome_map[end-1][1]) ||
		(ent.chromosome_map[end][2] == ent.chromosome_map[end-1][2] - 1 && 
		ent.chromosome_map[end][1] == ent.chromosome_map[end-1][1]) )   

	  get_this_bonus += 0.4
	else 
	  get_this_bonus -= 0.4
	end

	for i = 2:(size(ent.chromosome_map, 1) - 1)
	  if (ent.chromosome_map[i][1] == ent.chromosome_map[i+1][1] + 1  && 
		ent.chromosome_map[i][2] == ent.chromosome_map[i+1][2]) ||
		(ent.chromosome_map[i][1] == ent.chromosome_map[i+1][1] - 1 && 
		ent.chromosome_map[i][2] == ent.chromosome_map[i+1][2]) ||
		(ent.chromosome_map[i][2] == ent.chromosome_map[i+1][2] + 1 && 
		ent.chromosome_map[i][1] == ent.chromosome_map[i+1][1]) ||
		(ent.chromosome_map[i][2] == ent.chromosome_map[i+1][2] - 1 && 
		ent.chromosome_map[i][1] == ent.chromosome_map[i+1][1]) 

		get_this_bonus += 0.4
	  else 
		get_this_bonus -= 0.4
	  end
	  if (ent.chromosome_map[i][1] == ent.chromosome_map[i-1][1] + 1  && 
		  ent.chromosome_map[i][2] == ent.chromosome_map[i-1][2]) ||
		  (ent.chromosome_map[i][1] == ent.chromosome_map[i-1][1] - 1 && 
		  ent.chromosome_map[i][2] == ent.chromosome_map[i-1][2]) ||
		  (ent.chromosome_map[i][2] == ent.chromosome_map[i-1][2] + 1 && 
		  ent.chromosome_map[i][1] == ent.chromosome_map[i-1][1]) ||
		  (ent.chromosome_map[i][2] == ent.chromosome_map[i-1][2] - 1 && 
		  ent.chromosome_map[i][1] == ent.chromosome_map[i-1][1]) 

		get_this_bonus += 0.4
	  else 
		get_this_bonus -= 0.4
	  end
	end
	for i = 1:size(ent.chromosome_map, 1)
	  if ci_map.minimap[ent.chromosome_map[i][1], ent.chromosome_map[i][2]] == 0
		ent.map_hits += 1
	  else 
		ent.invalid_moves += 1
		get_this_bonus -= 0.2
	  end
	end
	end_index = 1 
	for i = 1:size(ent.chromosome_map, 1)
	  if ent.chromosome_map[i][1] == ci_map.end_position[1] &&
		ent.chromosome_map[i][2] == ci_map.end_position[2]
		end_index = i
		get_this_bonus += 0.4
		ent.target_cell_reached = true
	  end
	end
	get_this_bonus + ent.map_hits - end_index/100 #- ent.steps/10#- end_index
  end

  function group_entities(pop)
  #        println(pop[1].chromosome_map, " ", pop[1].fitness, ":", pop[1].steps, pop[1].map_hits )
	end_index = 0
	for i = 1:size(pop[1].chromosome_map, 1)
	  if pop[1].chromosome_map[i][1] == ci_map.end_position[1] &&
		  pop[1].chromosome_map[i][2] == ci_map.end_position[2]

		end_index = i
	  end
	end  
	if end_index != 0
	  correct_steps = 0
	  for i=1:(end_index - 1)
		if (pop[1].chromosome_map[i][1] ==  pop[1].chromosome_map[i+1][1] + 1 &&
		  pop[1].chromosome_map[i][2] ==  pop[1].chromosome_map[i+1][2] ) ||
		  (pop[1].chromosome_map[i][1] ==  pop[1].chromosome_map[i+1][1] - 1 &&
		  pop[1].chromosome_map[i][2] ==  pop[1].chromosome_map[i+1][2] ) ||

		  (pop[1].chromosome_map[i][1] ==  pop[1].chromosome_map[i+1][1] &&
		  pop[1].chromosome_map[i][2] ==  pop[1].chromosome_map[i+1][2] + 1 ) ||
		  (pop[1].chromosome_map[i][1] ==  pop[1].chromosome_map[i+1][1] &&
		  pop[1].chromosome_map[i][2] ==  pop[1].chromosome_map[i+1][2] - 1) 
		  correct_steps += 1
		end
	  end
	  if correct_steps == end_index - 1
		pop[1].end_index = end_index
		return
	  end
	end
	for i in 1:length(pop)
	  produce([1, i])
	end
  end

  function crossover(group)
	child = Crawler()
	num_parents = length(group)
	for i in 1:length(group[1].chromosome)
	  parent = (rand(UInt) % num_parents) + 1
	  child.chromosome[i] = group[parent].chromosome[i]
	end

	child
  end

  function mutate(ent)
	rand(Float64) < 0.98 && return

	some_elements = Int(floor(size(ci_map.minimap, 1)*size(ci_map.minimap, 2)/5))
	for i = 1:some_elements
	  rand_element = rand(1:(size(ci_map.minimap, 1)*size(ci_map.minimap, 2)))
	  ent.chromosome[rand_element ] = rand(1:4)
	end
  end


  function cells_to_moves(ent)
	c_map = ent.chromosome_map
# 	println(ent.chromosome)
	push!(ent.genetic_moves, "begin")
	for i=1:(ent.end_index - 1)
	  if c_map[i+1][1] == c_map[i][1] + 1 && c_map[i+1][2] == c_map[i][2]
	    push!(ent.genetic_moves, "move_right()")
	  elseif c_map[i+1][1] == c_map[i][1] - 1 && c_map[i+1][2] == c_map[i][2]
	    push!(ent.genetic_moves, "move_left()")
	  elseif c_map[i+1][1] == c_map[i][1] && c_map[i][2] + 1 == c_map[i+1][2]
	    push!(ent.genetic_moves, "move_forward()")
	  elseif c_map[i+1][1] == c_map[i][1] && c_map[i][2] - 1 == c_map[i+1][2]
	    push!(ent.genetic_moves, "move_backward()")
	  end
	end	  
    if (ent.target_cell_reached == true)
	    push!(ent.genetic_moves, "goal_reached()")

	end
	push!(ent.genetic_moves, "rescue => e")	
	push!(ent.genetic_moves, "println(e)")
	push!(ent.genetic_moves, "end")
	program_str = ""
 	for i=1:(size(ent.genetic_moves, 1))
          program_str = program_str * ent.genetic_moves[i]
    end
#     println(size(ent.genetic_moves, 1))
    ent.genetic_moves
  end
end 
