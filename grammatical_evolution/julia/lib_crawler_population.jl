include("lib_crawler_individual.jl")

type CrawlerPopulation <: Population
  individuals::Array{CrawlerIndividual, 1}

  function CrawlerPopulation(individuals::Array{CrawlerIndividual, 1})
    return new(copy(individuals))
  end

  function CrawlerPopulation(population_size::Int64, genome_size::Int64)
    individuals = Array(CrawlerIndividual, 0)
    for i=1:population_size
      push!(individuals, CrawlerIndividual(genome_size, 1000))
    end

    return new(individuals)
  end
end
