
type CrawlerIndividual <: Individual
  genome::Array{Int64, 1}
  fitness::Float64
  code
   
  map::Array{Float64, 2}
  invalid_moves::Int64
  x::Int64
  y::Int64

  function CrawlerIndividual(size::Int64, max_value::Int64)
    genome = rand(1:max_value, size)
    return new(genome, -1, nothing, zeros(7 ,3), 0, 0, 0)
  end

  CrawlerIndividual(genome::Array{Int64, 1}) = new(genome, -1, nothing, zeros(7 ,3), 0, 0, 0)
end
