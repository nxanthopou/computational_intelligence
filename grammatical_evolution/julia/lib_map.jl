
type Map
  width::Int64
  height::Int64
  value::Array{Int64, 2}
  start_pos::Array{Int64, 1}
  end_pos::Array{Int64, 1}
  
  #default constructor
  function Map(width::Int64, height::Int64)
    value = zeros(width, height)
    return new(width, height, value)
  end
   
  #constructor
  function Map(map::Array{Int64, 2}, start_pos::Array, end_pos::Array)
    return new(size(map, 1), size(map, 2), map, start_pos, end_pos)
  end
end
