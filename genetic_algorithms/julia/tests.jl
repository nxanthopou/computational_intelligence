#Xanthopoulos Nikitas
#02/05/2016
#Ergasia gia to ma8ima Ypologistiki 
#Noimosyni
#tests

#xrisimopoiw tin vivlio8iki
#https://github.com/WestleyArgentum/GeneticAlgorithms.jl


include("genetic_algorithms.jl")
include("ergasia.jl") 

using GeneticAlgorithms

function test_serial(pop_size)
  model = runga(ergasia; initial_pop_size = pop_size)
  return ergasia.cells_to_moves(population(model)[1])
#   return true
end
 
function test_parallel(n_procs, pop_size, timeout)
  results = parallel_comp(n_procs, pop_size, timeout)
  for i=1:size(results,1)
      println(results[i]) 
  end
#   return results
end
 
function parallel_comp(n_procs, pop_size, timeout)
  addprocs(n_procs)
  @everywhere include("tests.jl")
  max_runtime = timeout # secs
  tasks = []
  time_now = now()
  for k = 1:n_procs
	  push!(tasks, @spawn test_serial(pop_size))
  end
  println("****************************************")
  println("Created $(n_procs) threads..")
  println("Waiting results ($max_runtime seconds)..")
  running = true
  time_started = now()
  
  try
	while( running == true )
	  tasks_done = 0
	  for i=1:n_procs
	   if isready(tasks[i]) 
	     tasks_done += 1
       end
	  end
	  sleep(1)
	  if (now() - time_started )/1000 >= max_runtime || tasks_done == n_procs 
		running = false
	  end
	end 
  catch MethodError
# 	rmprocs(workers())
  end
  println("Total time: $(now() - time_now)")
  println("****************************************")
  resultsArrayStr = []
  for i=1:n_procs
   push!(resultsArrayStr, fetch(tasks[i]))
  end
  rmprocs(workers())
  return resultsArrayStr
#   sleep(max_runtime)
end
