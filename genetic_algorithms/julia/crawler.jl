#Xanthopoulos Nikitas
#02/05/2016
#Ergasia gia to ma8ima Ypologistiki 
#Noimosyni
#Klassi gia ka8e lysi tou GA

  using GeneticAlgorithms
  include("ci_map.jl")
  type Crawler <: Entity
    chromosome::Array
    steps::Int
    invalid_moves::Int
    fitness
    chromosome_map::Array
    map_hits::Int
    end_index::Int
    genetic_moves::Array{ASCIIString,1}
    target_cell_reached::Bool
    x::Int64
    y::Int64
	
    Crawler() = new(Array(Int, (size(ci_map.minimap, 1)*size(ci_map.minimap, 2))), 0, 0, nothing, [(ci_map.start_position[1], 
                   ci_map.start_position[2])], 0, 0, [], false, ci_map.start_position[1],
                   ci_map.start_position[2])
                   
    Crawler(chromosome) = new(chromosome, 0, 0, nothing, [(ci_map.start_position[1], 
                   ci_map.start_position[2])], 0, 0, [], false, ci_map.start_position[1],
                   ci_map.start_position[2])
                   
    
  end
